import {AfterViewInit, Component, OnInit} from '@angular/core';
import Swiper from 'swiper';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.scss']
})
export class AnimationComponent implements AfterViewInit {
  testSwiper: Swiper;
  slides = [
    'https://www.costco.ca/wcsstore/CostcoCABCCatalogAssetStore/homepage/d-hero-181126-cyber-monday-en.jpg',
    'https://www.costco.ca/wcsstore/CostcoCABCCatalogAssetStore/homepage/d-hero-181126-itunes-en.jpg',
    'https://www.costco.ca/wcsstore/CostcoCABCCatalogAssetStore/homepage/d-hero-181126-LG-en.jpg',
    'https://www.costco.ca/wcsstore/CostcoCABCCatalogAssetStore/homepage/d-hero-181126-vitamix-en.jpg',
    'https://www.costco.ca/wcsstore/CostcoCABCCatalogAssetStore/homepage/d-hero-181008-michelin-en.jpg',
    'https://www.costco.ca/wcsstore/CostcoCABCCatalogAssetStore/homepage/d-hero-181126-travel-en.jpg'
  ];
  constructor() {
    window.onload = function () {
      //initialize swiper when document ready
      let testSwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        mousewheelControl: true,
        paginationClickable: true,
        keyboardControl: true,

        // If we need pagination
        pagination: '.swiper-pagination',

        // Navigation arrows
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
      })
    };
  }

  // ngAfterViewInit() {
  //   this.testSwiper = new Swiper('.swiper-container', {
  //     direction: 'horizontal',
  //     loop: true,
  //     // 如果需要分页器
  //     pagination: {
  //       el: '.swiper-pagination',
  //     },
  //     // 如果需要前进后退按钮
  //     navigation: {
  //       nextEl: '.swiper-button-next',
  //       prevEl: '.swiper-button-prev',
  //     },
  //     // 如果需要滚动条
  //     scrollbar: {
  //       el: '.swiper-scrollbar',
  //     },
  //   });
  // }
}
